public class Trapezoid extends Shape{

    private int height;
    private int topEdge;
    private int bottomEdge;
    private int leftEdge;
    private int rightEdge;

    public Trapezoid(int height, int topEdge, int bottomEdge, int leftEdge, int rightEdge) {
        this.height = height;
        this.topEdge = topEdge;
        this.bottomEdge = bottomEdge;
        this.leftEdge = leftEdge;
        this.rightEdge = rightEdge;
    }

    @Override
    void Peremeter() {
        System.out.println("Chu vi hinh thang :" + (topEdge + bottomEdge + leftEdge + rightEdge));
    }

    @Override
    void Area() {
        System.out.println("Dien tich hinh thang: " + 0.5*(topEdge + bottomEdge)*height);
    }
}
