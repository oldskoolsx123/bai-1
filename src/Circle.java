public class Circle extends Shape {
    private double pi = 3.14;
    private int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    @Override
    void Peremeter() {
        System.out.println("Chu vi hinh tron :" + (2*pi*radius));
    }

    @Override
    void Area() {
        System.out.println("Dien tich hinh tron :" + ((radius*2)*3.14));
    }
}
