public class Rectangle extends Shape{
    private int length;
    private int width;

    public Rectangle(int length, int width) {
        this.length = length;
        this.width = width;
    }

    @Override
    void Peremeter() {
        System.out.println("Chu vi hinh chu nhat:" + (length+width)*2);
    }

    @Override
    void Area() {
        System.out.println("Dien tich hinh chu nhat:" + length*width);
    }
}
