public class Main {
    public static void main(String[] args) {
        Square square = new Square(10);
        square.Peremeter();
        square.Area();

        Rectangle rectangle = new Rectangle(10, 4);
        rectangle.Peremeter();
        rectangle.Area();

        Circle circle = new Circle(90);
        circle.Peremeter();
        circle.Area();

        Trapezoid trapezoid = new Trapezoid(7, 10, 6, 9, 9);
        trapezoid.Peremeter();
        trapezoid.Area();
    }
}