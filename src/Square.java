public class Square extends Shape{

    private int Edge;

    public Square(int edge) {
        Edge = edge;
    }

    @Override
    void Peremeter() {
        System.out.println("Chu vi hinh vuong :" + (Edge*4));
    }

    @Override
    void Area() {
        System.out.println("Chu vi hinh vuong :" + (Edge*Edge));
    }
}
